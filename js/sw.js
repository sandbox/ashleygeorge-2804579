
/* When Drupal serves this file at [basepath]/sw.js, it adds in some
 * contextual variables (like the base path name, and the no network page URL
 * from config) at the beginning of this file.
 *
 * Currently these variables are only used for excluding the sw.js file from the
 * cache - which I'm not sure is even necessary. But I'm leaving them in as we
 * will probably need a way to pass some data to the service worker further down
 * the line. Admittedly this way is crude, finding better alternatives is on my
 * radar.
 */

// Cache name.
var CACHE_NAME = 'no_t_rex_cache';

// Any resources to cache on install.
var urlsToCache = ['/no-network'];

// The insall listener. This runs when the service worker file is regestered in
// your browser. It gives us an opportunity to pre-cache any resources we want
// to be available off-line.
self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME).then(function(cache) {
      console.log('Opened cache');
      return cache.addAll(urlsToCache);
    })
  );
});

// The fetch listener. runs when the browser attempts to reach a resource. If
// the resource is in the cache we use that one. Otherwise, we fetch it and
// cache it.
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request).then(function(response) {
      console.log(event.request.url);
      // Cache hit - return response
      if (response) {
        return response;
      }

      // IMPORTANT: Clone the request. A request is a stream and
      // can only be consumed once. Since we are consuming this
      // once by cache and once by the browser for fetch, we need
      // to clone the request.
      var fetchRequest = event.request.clone();

      return fetchNewRequest(fetchRequest);
    })
  );
});

/**
 * Fetch the resource, maybe cache it, if there's no network display our cached
 * 'no-network' message.
 *
 * @param Object fetchRequest
 *   The request for a resource.
 * @returns Object
 *   The response, or the no-network page.
 */
function fetchNewRequest(fetchRequest) {
  return fetch(fetchRequest).then(
    function(response) {
      // Check if we received a valid response
      if(!response || response.status !== 200 || response.type !== 'basic') {
        return response;
      }

      // Don't put the spoof routed service worker in the cache. I'm not certain
      // that this is necessary but it seemed sensible to not put the service
      // worker in the cache that it manages itself.
      var SWUrl = baseUrl + '/sw.js';
      if (response.url == SWUrl) {
        return response;
      }

      // IMPORTANT: Clone the response. A response is a stream
      // and because we want the browser to consume the response
      // as well as the cache consuming the response, we need
      // to clone it so we have 2 stream.
      var responseToCache = response.clone();

      caches.open(CACHE_NAME).then(function(cache) {
        cache.put(fetchRequest, responseToCache);
      });

      return response;
    },
    // The requested page cannot be reached. So get the no-network page from
    // the cache.
    function() {
      return caches.match('/no-network').then(function(response) {
        return response;
      });
    }
  );
}