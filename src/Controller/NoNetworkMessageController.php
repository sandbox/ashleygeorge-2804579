<?php

namespace Drupal\no_t_rex\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableResponse;

/**
 * Controller generating a 'No Network' page. This page will be cached when the
 * service worker installs, and be displayed when the network is unavailable.
 */
class NoNetworkMessageController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // Get the message which should be presented to the user when network is
    // unavailable.
    $config = \Drupal::config('no_t_rex.settings');
    $nnmessage = $config->get('no_network_message');

    // Turn messgae into markup.
    $markup = '';
    if ($nnmessage) {
      $markup = "<h1>$nnmessage</h1>";
    }

    // Put message into a response object
    $html_response = new CacheableResponse($markup);
    $html_response->headers->set('Content-Type','text/html');
    return $html_response;
  }

}