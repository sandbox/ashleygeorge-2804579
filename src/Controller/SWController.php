<?php

namespace Drupal\no_t_rex\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Cache\CacheableResponse;

/**
 * Controller generating service worker at /sw.js.
 */
class SWController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    // Prepare some variables.
    $base_url = $GLOBALS['base_secure_url'];
    $module_path = drupal_get_path('module', 'no_t_rex');
    $url = $base_url . '/' . $module_path . '/js/sw.js';

    // Get the service worker script as a string.
    $client = \Drupal::httpClient();
    $guzzle_request = $client->get($url);
    $script_string = $guzzle_request->getBody(TRUE)->getContents();

    // Add the variables at the top of the script. This is a quick and dirty
    // way of getting custom data to the service worker. I would like this to be
    // better.
    $script_string = "var baseUrl = '$base_url';\r\n" . $script_string;
    $script_string = "var noTRexModulePath = '$module_path';\r\n" . $script_string;

    // Get the URL which should be cached to present to the user when network is
    // unavailable.
    $config = \Drupal::config('no_t_rex.settings');
    $nnmessage = $config->get('no_network_message');

    if ($nnmessage) {
      $script_string = "var noNetworkMessage = '$nnmessage';\r\n" . $script_string;
    }

    // Put into a response object
    $html_response = new CacheableResponse($script_string);
    $html_response->headers->set('Content-Type','text/javascript');
    return $html_response;
  }

}