<?php

/**
 * @file
 * Contains \Drupal\no_t_rex\Form\NoTRexSettingsForm
 */
namespace Drupal\no_t_rex\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure No T-Rex settings.
 */
class NoTRexSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'no_t_rex_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return array('no_t_rex.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('no_t_rex.settings');

    $form['no_network_message'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('No Network Message'),
      '#description' => $this->t('A message to display when no internet is available.'),
      '#default_value' => $config->get('no_network_message'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('no_t_rex.settings');
    $config->set('no_network_message', $form_state->getValue('no_network_message'))->save();

    parent::submitForm($form, $form_state);
  }

}